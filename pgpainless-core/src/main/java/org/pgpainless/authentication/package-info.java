// SPDX-FileCopyrightText: 2023 Paul Schaub <vanitasvitae@fsfe.org>
//
// SPDX-License-Identifier: Apache-2.0

/**
 * Classes and interfaces related to certificate authenticity.
 */
package org.pgpainless.authentication;
